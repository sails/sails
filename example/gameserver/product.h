#ifndef _PRODUCT_H_
#define _PRODUCT_H_

#include <map>
#include <string>

extern std::map<std::string, std::string> products_map;
extern std::map<std::string, std::string> crosslinks_map;


#endif /* _PRODUCT_H_ */
